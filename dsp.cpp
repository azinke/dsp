/**
 * @author: AMOUSSOU Z. Kenneth
 * @date: 08/31/2019
 * @version: 1.0
 */
#include "dsp.h"

/**
 * \brief Compute the mean of a sampled signal
 * @param data: array of measurement. sampled signal over a period
 * @param size: Number of data available. This is equuivalent to the number of
 * sample of data
 */
static float DSP::mean(float* data, uint16_t size){
  uint16_t i = 0;
  float result = 0.;
  for (i = 0; i < size; i++) {
    result += data[i];
  }
  return result / size;
}

/**
 * \brief Compute the energy of a sampled signal
 * @param data: array of measurement. Sampled signal over a period
 * @param size: Number of sample
 */
static float DSP::energy(float* data, uint16_t size){
  uint16_t i = 0;
  float result = 0.;
  for (i = 0; i < size; i++) {
    result += data[i] * data[i];
  }
  return result;
}

/**
 * \brief Compute the power of a sampled signal
 * @param data: array of measurement. Sampled signal over a period
 * @param size: Number of sample
 */
static float DSP::power(float* data, uint16_t size){
  float result = energy(data, size) / size;
  return result;
}

/**
 * \brief Compute the root mean square of a sampled signal
 * @param data: array of measurement. Sampled signal over a period
 * @param size: Number of sample
 */
static float DSP::rms(float* data, uint16_t size){
  return sqrt(power(data, size));
}

/**
 * \brief Compute the variance of a sampled signal
 * @param data: array of measurement. Sampled signal over a period
 * @param size: Number of sample
 */
static float DSP::variance(float* data, uint16_t size){
  float _mean = mean(data, size);
  return power(data, size) - _mean * _mean;
}

/**
 * \brief Find the maximum of a sampled signal given a dataset
 * @param data: array of measurement. Sampled signal over a period
 * @param size: Number of sample
 */
static float DSP::maximun(float* data, uint16_t size){
  uint16_t i = 0;
  float maxi = data[0];
  for (i = 1; i < size; i++) {
    if (maxi < data[i]) maxi = data[i];
  }
  return maxi;
}

/**
 * \brief Find the minimum of a sampled signal given a dataset
 * @param *data: array of measurement. Sampled signal over a period
 * @param size: Number of sample
 */
static float DSP::minimum(float *data, uint16_t size){
  uint16_t i = 0;
  float mini = data[0];
  for (i = 1; i < size; i++) {
    if (mini > data[i]) mini = data[i];
  }
  return mini;
}
