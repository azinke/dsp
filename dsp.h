/**
 * @author: AMOUSSOU Z. Kenneth
 * @date: 08/31/2019
 * @version: 1.0
 */
#ifndef H_DSP
#define H_DSP

#include <math.h>
#include <stdio.h>

class DSP {
  public:
    DSP(){  };
    // public methods
    // Let's understood that numeric signal is equivalent to sampled signal

    // compute the mean of a signal given it's sampled data
    static float mean(float* data, uint16_t size);
    
    // compute the energy of a numeric signal
    static float energy(float* data, uint16_t size);

    // compute the power of a numeric signal
    static float power(float* data, uint16_t size);

    // compute the root mean square of a numeric signal
    static float rms(float* data, uint16_t size);

    // compute the variance of a numeric signal
    static float variance(float* data, uint16_t size);

    // find the maximum of a sampled signal given a dataset
    static float maximun(float* data, uint16_t size);
 
    // find the minimum of a sampled signal given a dataset
    static float minimum(float* data, uint16_t size);

  private:
    // private methods and vars
};

#endif
